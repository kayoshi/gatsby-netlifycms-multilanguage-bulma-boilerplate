module.exports = {
  de: {
    path: 'de',
    locale: 'German',
    default: true,
  },
  en: {
    path: 'en',
    locale: 'English',
    default: false,
  },
}
