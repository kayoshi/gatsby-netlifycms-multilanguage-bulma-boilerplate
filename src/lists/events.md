---
events:
  - date: 2020-11-29T23:00:00.000Z
    name: First event
    image: /images/01.png
  - date: 2020-11-29T23:00:00.000Z
    name: Second event
    image: /images/02.png
  - date: 2020-12-07T23:00:00.000Z
    name: Third event
    image: /images/03.png
---
