import React, { useEffect } from 'react'
import { useStaticQuery, Link, graphql } from 'gatsby'
import { i18n } from '../constants/i18n'
import animateNavbar from '../scripts/animate-navbar.js'
const locales = require('../constants/locales')

export default function Layout({ children, locale }) {
  const data = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
          }
        }
        allMarkdownRemark(
          filter: {
          	fileAbsolutePath: {regex: "/(pages)/" },
          }
        ){
          edges {
            node {
              frontmatter {
                title
                locale
              }
              fields {
                slug
              }
            }
          }
        }
      }
    `
  )

  useEffect(() => {
    // Run vendor scripts after DOM rendered
    animateNavbar()
  }, []);

  function localePath(locale, slug) {
    const l = locales[locale]
    return l.default ? slug : '/' + l.path + slug
  }

  const PageLinks = () => (
    <div className="navbar-end">
      {data.allMarkdownRemark.edges.map(({ node }) => {
        // skip root link and those from other locale
        if (node.fields.slug === '/' || node.frontmatter.locale !== locale) { return false }

        return (
          <Link
            key={node.fields.slug}
            to={localePath(locale, node.fields.slug)}
            className='navbar-item'
          >
            {node.frontmatter.title}
          </Link>
        )
      })}
    </div>
  )

  return (
    <>
      <nav className="navbar" role="navigation" aria-label="main navigation">
        <div className='container'>
          <div className="navbar-brand">
            <Link to={localePath(locale, '/')} className='navbar-item'>{i18n[locale]['title']}</Link>
            {Object.keys(locales).map(lang => (
              <Link
                key={lang}
                to={localePath(lang, '/')}
                className={`navbar-item locale-switch ${lang === locale ? 'active' : ''}`}
              >
                {lang}
              </Link>
            ))}

            <a role="button" href='#' className="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="theNavbar">
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
            </a>
          </div>

          <div id="theNavbar" className="navbar-menu">
            <PageLinks />
          </div>
        </div>
      </nav>
      {children}
    </>
  )
}
