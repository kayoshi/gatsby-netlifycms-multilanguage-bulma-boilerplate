import React, { useState } from 'react'
import { graphql } from 'gatsby'
import Layout from '../components/layout'

export default function Home({ data, pageContext }) {
  const md = data.markdownRemark
  const events = data.allMarkdownRemark.edges[0].node.frontmatter.events
  const [event, setEvent] = useState(null);

  const Event = (props) => {
    const active = event === props.event.name

    return <div
      role='button'
      tabIndex={0}
      className='event-tile'
      onKeyDown={() => setEvent(props.event.name)}
      onClick={() => setEvent(props.event.name)}
    >
      <div><img src={props.event.image} alt={'Image for ' + props.event.name}/></div>
      {active ? <div className='details'>{props.event.name}</div> : ''}
    </div>
  }

  return (
    <Layout locale={pageContext.locale}>
      <section className='section'>
        <div className='container'>
          <div className='content' dangerouslySetInnerHTML={{ __html: md.html }}/>
        </div>
      </section>

      <section className='section'>
        <div className='container'>
          <div className='columns is-multiline'>
            {events.map(event => <Event event={event} key={event.name} />)}
          </div>
        </div>
      </section>
    </Layout>
  )
}

export const query = graphql`
  query IndexPage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      html
      frontmatter {
        title
      }
    }
    allMarkdownRemark(filter: {fileAbsolutePath: {regex: "/(lists/events)/"}}) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            events {
              name
              image
            }
          }
        }
      }
    }
  }
`
