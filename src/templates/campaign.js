import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../components/layout'

export default function Campaign({ data, pageContext }) {
  const md = data.markdownRemark

  return (
    <Layout locale={pageContext.locale}>
      <section className='section'>
        <div className='container'>
          <h1 className='title is-1'>{md.frontmatter.title}</h1>
          <div className='content' dangerouslySetInnerHTML={{ __html: md.html }} />
        </div>
      </section>
    </Layout>
  )
}

export const query = graphql`
  query Campaign($id: String!) {
    markdownRemark(id: { eq: $id }) {
      html
      frontmatter {
        title
      }
    }
  }
`
