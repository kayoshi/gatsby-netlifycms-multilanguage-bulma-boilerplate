module.exports = {
  siteMetadata: {
    title: 'FOOBAR',
  },
  plugins: [
    'gatsby-plugin-sass',
    `gatsby-transformer-remark`,
    'gatsby-plugin-netlify-cms',
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `src`,
        path: `${__dirname}/src/`,
      },
    },
    {
      resolve: `gatsby-plugin-react-leaflet`,
      options: {
        linkStyles: false
      }
    },
  ],
}
