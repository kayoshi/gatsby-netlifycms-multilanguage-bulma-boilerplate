const path = require('path')
const { createFilePath } = require('gatsby-source-filesystem')
const locales = require('./src/constants/locales')

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions
  if (node.internal.type === 'MarkdownRemark') {
    let slug = createFilePath({ node, getNode, basePath: 'pages' })
    Object.keys(locales).forEach(locale => {
      slug = slug.replace(`.${locale}`, '')
    });

    createNodeField({
      node,
      name: 'slug',
      value: slug == '/index/' ? '/' : slug,
    })
  }
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const result = await graphql(`
    query {
      allMarkdownRemark {
        edges {
          node {
            id
            fields {
              slug
            }
            frontmatter {
              templateKey
              locale
            }
          }
        }
      }
    }
  `)
  result.data.allMarkdownRemark.edges.forEach(({ node }) => {
    if (node.frontmatter.templateKey && node.frontmatter.locale) {
      const locale = locales[node.frontmatter.locale]
      const localizedPath = locale.default ? node.fields.slug : locale.path + node.fields.slug

      createPage({
        path: localizedPath,
        component: path.resolve(
          `src/templates/${String(node.frontmatter.templateKey)}.js`
        ),
        context: {
          // Data passed to context is available
          // in page queries as GraphQL variables.
          locale: locale.path,
          id: node.id
        },
      })
    }
  })
}
